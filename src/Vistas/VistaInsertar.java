package Vistas;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class VistaInsertar extends Vista {
	
	private String tipo;
	
	public VistaInsertar(String tipo) {
		this.tipo = tipo;
	}
	
	@Override public Vista armar() {
		
		JLabel lblInsertar = new JLabel("Insertar " + this.tipo);
		lblInsertar.setBounds(10, 11, 100, 14);
		this.add(lblInsertar);
		
		JTextField inputInsertarNombres = new JTextField();
		inputInsertarNombres.setBounds(10, 29, 185, 20);
		this.add(inputInsertarNombres);
		inputInsertarNombres.setColumns(10);
		
		JButton btnInsertar = new JButton("Insertar " + this.tipo);
		btnInsertar.setBounds(10, 54, 185, 23);
		this.add(btnInsertar);
		
		return this;
	}

}
