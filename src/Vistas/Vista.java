package Vistas;

import javax.swing.JPanel;
import Controladores.Controlador;

// Clase abstracta para construcción generica de vistas

public abstract class Vista extends JPanel { 
	protected Controlador controlador;

	public abstract Vista armar();
	
	public void agregarControlador(Controlador c) {
		this.controlador = c;
	}
}
