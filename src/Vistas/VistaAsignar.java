package Vistas;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;

public class VistaAsignar extends Vista {
	
	@Override public Vista armar() {
		
		// Hardcoding solo para test
		
		DefaultListModel<String> listModel = new DefaultListModel<>();
        listModel.addElement("USA");
        listModel.addElement("India");
        listModel.addElement("Vietnam");
        listModel.addElement("Canada");
        listModel.addElement("Denmark");
        listModel.addElement("France");
        listModel.addElement("Great Britain");
        listModel.addElement("Japan");	
		
		JLabel lblListadoDePersonas = new JLabel("Listado de Personas");
		this.add(lblListadoDePersonas);
		
		JList list = new JList(listModel);
		this.add(list);
		
		JLabel lblListaDeTareas = new JLabel("Listado de Tareas");
		this.add(lblListaDeTareas);
		
        JList list_1 = new JList<>(listModel);
		this.add(list_1);
		
		return this;
	}
	
}
