package Vistas;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class VistaAsociar extends Vista {
    // Codigo tomado del archivo anterior
	
	@Override public Vista armar() {
			
		JLabel lblAsociarPersonasA = new JLabel("Asociar Personas a Tareas");
		lblAsociarPersonasA.setBounds(10, 11, 139, 14);
		this.add(lblAsociarPersonasA);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(10, 30, 159, 20);
		this.add(comboBox);
		
		JButton btnNewButton = new JButton("< - >");
		btnNewButton.setBounds(179, 29, 72, 23);
		this.add(btnNewButton);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(261, 30, 159, 20);
		this.add(comboBox_1);	
		
		return this;
	}
	
}
