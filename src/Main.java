import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.JFrame;

import Controladores.ControladorAsignar;
import Controladores.ControladorAsociar;
import Controladores.ControladorInsertar;
import Vistas.VistaAsignar;
import Vistas.VistaAsociar;
import Vistas.VistaInsertar;

public class Main {
	
	// Configuración de la app, simple y sencillo
	
	private static void boot() {
		JFrame frame = new JFrame("Tarea paradigmas");
		
		// Creación de modelos
		
		ArrayList<Object> personas = new ArrayList<>();
		ArrayList<Object> tareas = new ArrayList<>();
		
		// Creación de controladores
		
		ControladorAsignar cAsignar = new ControladorAsignar();
		ControladorAsociar cAsociar = new ControladorAsociar();
		ControladorInsertar cInsertarPersonas = new ControladorInsertar();
		ControladorInsertar cInsertarTareas = new ControladorInsertar();
		
		// Asignacion de modelos para los controladores
		
		cAsignar.agregarModelo(tareas);
		cAsociar.agregarModelo(personas);
		cInsertarPersonas.agregarModelo(personas);
		cInsertarTareas.agregarModelo(tareas);
		
		
		// Creación de vistas
		
		VistaInsertar vistaInsertarPersonas = new VistaInsertar("personas");
		VistaInsertar vistaInsertarTareas = new VistaInsertar("tareas");
		VistaAsociar asociarTareasPersonas = new VistaAsociar();
		VistaAsignar asignarTareasPersonas = new VistaAsignar(); 
		
		// Asignacion de controladores para las vistas
		
		vistaInsertarPersonas.agregarControlador(cInsertarPersonas);
		vistaInsertarTareas.agregarControlador(cInsertarTareas);
		asociarTareasPersonas.agregarControlador(cAsociar);
		asignarTareasPersonas.agregarControlador(cAsignar);
		
		frame.getContentPane().add(vistaInsertarPersonas.armar());
		frame.getContentPane().add(vistaInsertarTareas.armar());
		frame.getContentPane().add(asociarTareasPersonas.armar());
		frame.getContentPane().add(asignarTareasPersonas.armar());
		
		frame.getContentPane().setPreferredSize(new Dimension(800,300));
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.pack();
		frame.setVisible(true);
	}
	
	// Inicio de la app
	
	public static void main(String[] args) {
		 EventQueue.invokeLater(new Runnable(){
		     public void run(){
		         try {
		             boot();
		         } catch(Exception e){
		             e.printStackTrace();
		         }
		     }
		 });
	}

}
